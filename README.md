# Lehigh University CSE262 - Programming Languages - Homework 5

Solve the following ten questions regarding the Lambda Calculus.

## Question 1

Make all parentheses explicit in these λ- expressions:

1. (λp.pz) λq.w λw.wqzp
 (λp.(pz) λq(.w λw.(((wq)z)p)))

2. λp.pq λp.qp
λp.(pq λp.(qp))

## Question 2

In the following expressions say which, if any, variables are bound (and to which λ), and which are free.

1. λs.s z λq.s q

From left to right:
λs and s are bound, z is free, λq and s are bound, q is free.

2. (λs. s z) λq. w λw. w q z s
λs and s are bound. z is free. λq and w are free, λw and w are bound, q is bound, z and s are free.


3. (λs.s) (λq.qs)
λs and s are bound, λq and q are bound, s is free.

4. λz. (((λs.sq) (λq.qz)) λz. (z z))
λz is free, λs.sq is bound, λq.q is bound, but the last two are free. The z's at the end are bound, including λz


## Question 3

Put the following expressions into beta normal form (use β-reduction as far as possible, α-conversion as needed) assuming left-association.

1. (λz.z) (λq.q q) (λs.s a)
[z--> λq.qq]
(λq . qq) (λs . sa)
[q --> (λs . sa)]
(λs.sa)(λs.sa)
(s--> λs.sa)
(λs . sa)a
[s--> a]
aa

2. (λz.z) (λz.z z) (λz.z q)
(λz.z) (λz'.z'z')(λz''.z''q)
[z --> (λz' . z' z')]
(λz'.z'z')(λz''.z''q) [z' --> (λz'' . z''q)]
(λz''.z''q) (λz'''.z'''q') [z'' --> (λz'''.z'''q')]
(λz''.z''q) q [z''-->q]
qq

3. (λs.λq.s q q) (λa.a) b
(s-->(λa.a))]
(λq . (λa.a) q q)b
[q--> b]
(λa.a)bb
[a-->b]
bb

4. (λs.λq.s q q) (λq'.q') q''
[s--> (λq' . q')]
(λq . (λq'.q') q q)q''
[q --> q'']
(λq'.q') q''q''
[q' --> q'']
q''q''


5. ((λs.s s) (λq.q)) (λq'.q')
[ s --> (λq . q)]
((λq . q)(λq . q)) (λq' . q')
[q --> q]
(λq . q) (λq' . q')
[q --> (λq' . q')]
(λq'.q')


## Question 4

1. Write the truth table for the or operator below.

TF --> T
TT --> T
FT --> T
FF --> F

2. The Church encoding for OR = (λp.λq.p p q)

Prove that this is a valid "or" function by showing that its output matches the truth table above. You will have 4 derivations. For the first derivation, show the long-hand solution (don't use T and F, use their definitions). For the other 3 you may use the symbols in place of the definitions. 

4 Derivations:

OR TT:

(λp.λq.p p q) (λpλq.p) (λpλq.p)
(λq.p p q) [p->λpλq.p] (λpλq.p)
(λq.(λpλq) (λpλq.p) (q)) (λpλq.p)
(λpλq.p) (λpλq.p) (λpλq.p)
(λq.p) [p->λpλq.p] (λpλq.p)
(λq.(λpλq.p)) (λpλq.p)
(λq.(λpλq.p)) [q->λpλq.p]
(λpλq.p) = T

OR TF:

(λp.λq.p p q)TF
(λq.p p q)[p->T]F
(λq.T T q)F
(T T q) [q->F]
(T T F) = T

OR FT:

(λp.λq.p p q)FT
(λq.p p q)[p->F]T
(λq.F F q)T
(F F q) [q->T]
(F F T) = T

OR FF:
(λp.λq.p p q)FF
(λq.p p q)[p->F]F
(λq.F F q)F
(F F q) [q->F]
(F F F) = F


## Question 5

Derive a lambda expression for the NOT operator. Explain how this is similar to an IF statement.

Conceptually:

-We want: NOT True
-( λ y . y FALSE TRUE) TRUE
-TRUE FALSE TRUE
( λ x . λ y y)


Lambda:

( λ y . y ( λ x . λ y x) ( λ x . λ y y)) 

( λ x . λ y x) ( λ x . λ y y) ( λ x . λ y x)

( λ x . λ y . x ) ( λ x . λ y y) ( λ x . λ y x)

( λ x . λ y y)

It's similar to an IF statement because it is logical negation, or the opposite of an if statement. In an IF statement, the second value is chosen, however in a NOT statement 
the first statement is chosen.




## Instructions

1. Fork the relevant repository into your own namespace. [Instructions](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Set your forked repository visibility to private. [Instructions](https://docs.gitlab.com/ee/public_access/public_access.html#how-to-change-project-visibility)
3. Add user "LehighCSE262" as a member to the project with "maintainer" access-level. [Instructions](https://docs.gitlab.com/ee/user/project/members/#add-a-user). 
4. Clone your newly forked repository. [Instructions](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository) 
5. Answer the questions here in the readme or in another document. Upload your solutions here to Gitlab.
